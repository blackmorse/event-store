package com.blackmorse.Utils;

import com.blackmorse.commands.statistics.Period;
import io.vertx.core.AsyncResult;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by black on 16.07.2017.
 */
public final class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    private Utils() {
    }

    public static Integer getCurrentTime() {
        return Math.toIntExact(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
    }

    public static Integer getStartTime(Period period) {
        return getCurrentTime() - period.getSeconds();
    }

    public static <T> AsyncResult<T> createFailed(Throwable cause) {
        return new AsyncResult<T>() {
            @Override
            public T result() {
                return null;
            }

            @Override
            public Throwable cause() {
                return cause;
            }

            @Override
            public boolean succeeded() {
                return true;
            }

            @Override
            public boolean failed() {
                return false;
            }
        };
    }

    public static <T> AsyncResult<T> createSuccess(T result) {
        return new AsyncResult<T>() {
            @Override
            public T result() {
                return result;
            }

            @Override
            public Throwable cause() {
                return null;
            }

            @Override
            public boolean succeeded() {
                return true;
            }

            @Override
            public boolean failed() {
                return false;
            }
        };
    }
}
