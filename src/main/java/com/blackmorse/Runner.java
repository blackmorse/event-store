package com.blackmorse;

import com.blackmorse.commands.ActionWorkerServiceVertical;
import com.blackmorse.persistence.PersistenceServiceVerticle;
import com.blackmorse.web.WebVerticle;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

/**
 *
 */
public class Runner extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(Runner.class);

    public static void main(String[] args) {
        ClusterManager mgr = new HazelcastClusterManager();
        VertxOptions options = new VertxOptions().setClusterManager(mgr);

        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx vertx = res.result();
                JsonObject configs = new JsonObject();
                configs.put("http.port", "8088");
                vertx.deployVerticle(Runner.class.getName(), new DeploymentOptions(configs));
            } else {
                log.error("Failed to start the app");
            }
        });
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        final DeploymentOptions options = new DeploymentOptions().setConfig(config());
        startPersistence((none0) -> {
            startBackend((none1) -> {
                startWebApp(
                        (none2) -> {
                            completeStartup(none2, startFuture);
                        }, options, startFuture
                );
            }, options.setWorker(true).setInstances(2), startFuture);
        }, options, startFuture);
    }

    private void startPersistence(AsyncResultHandler<Void> next, DeploymentOptions options, Future<Void> future) {
        vertx.deployVerticle(PersistenceServiceVerticle.class.getName(), options, res -> {
            if (res.succeeded()) {
                next.handle(Future.succeededFuture());
            } else {
                log.error("PersistenceVerticle deployment failed!" + res.cause());
                future.fail(res.cause());
            }
        });
    }

    private void startBackend(AsyncResultHandler<Void> next, DeploymentOptions options, Future<Void> future) {
        vertx.deployVerticle(ActionWorkerServiceVertical.class.getName(), options, res -> {
            if (res.succeeded()) {
                next.handle(Future.succeededFuture());
            } else {
                log.error("ActionWorker deployment failed!" + res.cause());
                future.fail(res.cause());
            }
        });
    }

    private void startWebApp(AsyncResultHandler<Void> next, DeploymentOptions options, Future<Void> future) {
        vertx.deployVerticle(WebVerticle.class.getName(), options, res -> {
            if (res.succeeded()) {
                next.handle(Future.succeededFuture());
            } else {
                log.error("WebVerticle deployment failed!" + res.cause());
                future.fail(res.cause());
            }
        });
    }

    private void completeStartup(AsyncResult<Void> res, Future<Void> fut) {
        if (res.succeeded()) {
            log.info("Application started");
            fut.complete();
        } else {
            fut.fail(res.cause());
        }
    }
}
