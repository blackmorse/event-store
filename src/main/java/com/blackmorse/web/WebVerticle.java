package com.blackmorse.web;

import com.blackmorse.Utils.Constansts;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * Created by black on 15.07.2017.
 */
public class WebVerticle extends AbstractVerticle {
    private static final String HANDLER = "/handler";

    @Override
    public void start(Future<Void> fut) {
        int port = config().getInteger("http.port", 8088);
        Router router = Router.router(vertx);
        final DeliveryOptions opts = new DeliveryOptions();

        router.route().handler(BodyHandler.create());
        router.route(HANDLER).
                handler(routingContext -> {
                    vertx.eventBus().<JsonObject>send(Constansts.SERVICE_WORKER, routingContext.getBodyAsJson(), opts, reply -> handleReply(reply, routingContext));
                });
        vertx.createHttpServer().
                requestHandler(router::accept).listen(
                port,
                result -> {
                    if (result.succeeded()) fut.complete();
                    else fut.fail(result.cause());
                });
    }


    private void handleReply(AsyncResult<Message<JsonObject>> reply, RoutingContext rc) {
        if (reply.succeeded()) {
            Message<JsonObject> replyMsg = reply.result();
            rc.response()
                    .setStatusMessage("OK")
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("Content-Type", "application/json")
                    .end(replyMsg.body().toString());
        } else {
            rc.response()
                    .setStatusCode(HttpResponseStatus.BAD_REQUEST.code())
                    .setStatusMessage("Invalid request")
                    .end(reply.cause().getLocalizedMessage());
        }
    }
}
