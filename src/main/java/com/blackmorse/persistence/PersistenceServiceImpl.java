package com.blackmorse.persistence;

import com.blackmorse.Utils.Constansts;
import com.blackmorse.Utils.Utils;
import com.blackmorse.commands.statistics.Period;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.impl.VertxInternal;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;

import java.util.Map;

/**
 * Created by black on 15.07.2017.
 */
public class PersistenceServiceImpl implements PersistenceService {
    private static final Logger log = LoggerFactory.getLogger(PersistenceServiceImpl.class);

    private final ClusterManager clusterManager;

    public PersistenceServiceImpl(Vertx vertx) {
        this.clusterManager = ((VertxInternal) vertx).getClusterManager();
    }

    @Override
    public void registerEvent(Handler<AsyncResult<Integer>> handler) {
        Integer currentTime = Utils.getCurrentTime();
        clusterManager.getLockWithTimeout(currentTime.toString(), timeOut, lockAsyncResult -> {
            AsyncResult<Integer> result = null;
            try {
                if (lockAsyncResult.failed()) {
                    result = Utils.createFailed(lockAsyncResult.cause());
                } else {
                    Map<Integer, Integer> map = clusterManager.getSyncMap(Constansts.EVENT_MAP);
                    Integer value = map.getOrDefault(currentTime, 0);
                    map.put(currentTime, ++value);
                    result = Utils.createSuccess(value);
                }
            } catch (Exception e) {
                result = Utils.createFailed(e);
            } finally {
                lockAsyncResult.result().release();
            }
            handler.handle(result);
        });
    }

    @Override
    public void statistics(Period period, Handler<AsyncResult<Long>> handler) {
        AsyncResult<Long> asyncResult = null;
        try {
            Integer startTime = Utils.getStartTime(period);
            Integer currentTime = Utils.getCurrentTime();

            long result = 0;
            Map<Integer, Integer> map = clusterManager.getSyncMap(Constansts.EVENT_MAP);

            for (int i = startTime; i < currentTime; i++) {
                long value = (long) map.getOrDefault(i, 0);
                result += value;
            }
            Utils.createSuccess(result);
        } catch (Exception e) {
            Utils.createFailed(e);
        }
        handler.handle(asyncResult);
    }
}

