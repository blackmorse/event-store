package com.blackmorse.persistence;

import com.blackmorse.commands.statistics.Period;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.serviceproxy.ProxyHelper;

/**
 * Created by black on 15.07.2017.
 */
@VertxGen
@ProxyGen
public interface PersistenceService {
    long timeOut = 5000;

    static PersistenceService create(Vertx vertx) {
        return new PersistenceServiceImpl(vertx);
    }

    static PersistenceService createProxy(Vertx vertx, String address) {
        return ProxyHelper.createProxy(PersistenceService.class, vertx, address);
    }

    public void registerEvent(Handler<AsyncResult<Integer>> handler);

    public void statistics(Period period, Handler<AsyncResult<Long>> handler);

}


