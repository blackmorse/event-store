package com.blackmorse.persistence;

import com.blackmorse.Utils.Constansts;
import io.vertx.core.AbstractVerticle;
import io.vertx.serviceproxy.ProxyHelper;


/**
 * Created by black on 15.07.2017.
 */
public class PersistenceServiceVerticle extends AbstractVerticle {
    private PersistenceService service;

    @Override
    public void start() throws Exception {
        service = PersistenceService.create(vertx);
        ProxyHelper.registerService(
                PersistenceService.class, vertx, service,
                Constansts.SERVICE_PERSISTENCE);
    }
}
