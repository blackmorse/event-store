package com.blackmorse.commands.statistics;

/**
 * Created by black on 16.07.2017.
 */
public enum Period {
    MINUTE(60),
    HOUR(60 * 60),
    DAY(60 * 60 * 24);

    private Integer seconds;

    private Period(Integer seconds) {
        this.seconds = seconds;
    }

    public Integer getSeconds() {
        return seconds;
    }
}
