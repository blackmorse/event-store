package com.blackmorse.commands.statistics;

import com.blackmorse.Utils.Constansts;
import com.blackmorse.Utils.Utils;
import com.blackmorse.commands.Action;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Collections;
import java.util.Map;

/**
 * Created by black on 15.07.2017.
 */
public class StatisticsAction extends Action {
    private final static Logger log = LoggerFactory.getLogger(StatisticsAction.class);

    private final Period period;

    public StatisticsAction(Vertx vertx, Period period) {
        super(vertx);
        this.period = period;
    }

    @Override
    public void handle(Message<JsonObject> jsonObjectMessage) {

        Integer startTime = Utils.getStartTime(period);
        Integer currentTime = Utils.getCurrentTime();

        long result = 0;
        Map<Integer, Integer> map = clusterManager.getSyncMap(Constansts.EVENT_MAP);

        for (int i = startTime; i < currentTime; i++) {
            long value = (long) map.getOrDefault(i, 0);
            result += value;
        }
        JsonObject response = new JsonObject(Collections.singletonMap(period.name(), result));
        log.info("Stats for period {0} : {1}", period.name(), result);
        jsonObjectMessage.reply(response);
    }
}
