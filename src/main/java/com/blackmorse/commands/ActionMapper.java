package com.blackmorse.commands;

import com.blackmorse.commands.register.RegisterAction;
import com.blackmorse.commands.statistics.Period;
import com.blackmorse.commands.statistics.StatisticsAction;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by black on 16.07.2017.
 */
public class ActionMapper {

    private static final Map<String, Action> map = new HashMap<>();

    private final Action NOT_FOUND_ACTION;

    public ActionMapper(Vertx vertx) {
        map.put("register", new RegisterAction(vertx));
        map.put("statisticMinute", new StatisticsAction(vertx, Period.MINUTE));
        map.put("statisticHour", new StatisticsAction(vertx, Period.HOUR));
        map.put("statisticDay", new StatisticsAction(vertx, Period.DAY));
        NOT_FOUND_ACTION = new Action(vertx) {
            @Override
            public void handle(Message<JsonObject> jsonObjectMessage) {
                jsonObjectMessage.fail(HttpResponseStatus.NOT_IMPLEMENTED.code(), "Invalid action");
            }
        };
    }

    public Action getAction(String address) {
        return map.getOrDefault(address, NOT_FOUND_ACTION);
    }
}
