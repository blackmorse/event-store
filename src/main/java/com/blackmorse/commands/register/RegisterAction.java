package com.blackmorse.commands.register;

import com.blackmorse.Utils.Utils;
import com.blackmorse.commands.Action;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Collections;

/**
 * Created by black on 15.07.2017.
 */
public class RegisterAction extends Action {
    private static final Logger log = LoggerFactory.getLogger(RegisterAction.class);

    public RegisterAction(Vertx vertx) {
        super(vertx);
    }

    @Override
    public void handle(Message<JsonObject> jsonObjectMessage) {

        Integer currentTime = Utils.getCurrentTime();

        persistenceService.registerEvent((result) -> {
            if (result.succeeded()) {
                JsonObject response = new JsonObject(Collections.singletonMap(currentTime.toString(), result.result().toString()));
                log.info("Entries for time {0} : {1}", currentTime, result.result());
                jsonObjectMessage.reply(response);
            } else {
                log.error("Connection or Operation Failed " + result.cause());
                jsonObjectMessage.fail(HttpResponseStatus.INTERNAL_SERVER_ERROR.code(), "Connection or Operation Failed " + result.cause());
            }
        });


    }
}