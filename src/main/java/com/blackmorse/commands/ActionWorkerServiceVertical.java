package com.blackmorse.commands;

import com.blackmorse.Utils.Constansts;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

/**
 * Created by black on 15.07.2017.
 */
public class ActionWorkerServiceVertical extends AbstractVerticle {
    private static final String ACTION = "action";

    @Override
    public void start() throws Exception {
        ActionMapper actionMapper = new ActionMapper(vertx);
        vertx.eventBus().<JsonObject>consumer(Constansts.SERVICE_WORKER).handler(message -> {
            if (!message.body().containsKey(ACTION)) {
                        message.fail(HttpResponseStatus.BAD_REQUEST.code(), "Empty action");
            } else {
                actionMapper.getAction(String.valueOf(message.body().getValue(ACTION))).handle(message);
                    }
                }
        );
    }
}