package com.blackmorse.commands;

import com.blackmorse.Utils.Constansts;
import com.blackmorse.persistence.PersistenceService;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.impl.VertxInternal;
import io.vertx.core.json.JsonObject;
import io.vertx.core.spi.cluster.ClusterManager;

/**
 * Created by black on 16.07.2017.
 */
public abstract class Action implements Handler<Message<JsonObject>> {

    protected final PersistenceService persistenceService;

    protected final ClusterManager clusterManager;

    public Action(Vertx vertx) {
        this.persistenceService = PersistenceService.createProxy(vertx, Constansts.SERVICE_PERSISTENCE);
        this.clusterManager = ((VertxInternal) vertx).getClusterManager();
    }
}
